# the player must guess a randomly-selected five-letter word in six tries
# each try is a five-letter word. after each try, each letter in the try is
# assigned an indication of its position in the word to be guessed. a letter is
# either in the word to be guessed or not. if it is in the word, then it is
# either in the correct spot (in the same spot as in the word to be guessed) or
# not.

import tkinter as tk
import tkinter.messagebox as messagebox
import random

keyboard = [
    ["Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P"],
    ["A", "S", "D", "F", "G", "H", "J", "K", "L"],
    ["Enter", "Z", "X", "C", "V", "B", "N", "M", "Back"]
]

WORD_LIST = ["ABOUT", "PILLS", "WEARY", "VAGUE", "ROUND", "QUEEN"]

def main():
    root = tk.Tk()
    word_to_guess = random.choice(WORD_LIST)
    position = (0, 0)
    is_won = False
    # the variables containing each tile's letter - when the user clicks the
    # keyboard, these are updated
    tile_vars = [
        [tk.StringVar(), tk.StringVar(), tk.StringVar(), tk.StringVar(), tk.StringVar()],
        [tk.StringVar(), tk.StringVar(), tk.StringVar(), tk.StringVar(), tk.StringVar()],
        [tk.StringVar(), tk.StringVar(), tk.StringVar(), tk.StringVar(), tk.StringVar()],
        [tk.StringVar(), tk.StringVar(), tk.StringVar(), tk.StringVar(), tk.StringVar()],
        [tk.StringVar(), tk.StringVar(), tk.StringVar(), tk.StringVar(), tk.StringVar()],
        [tk.StringVar(), tk.StringVar(), tk.StringVar(), tk.StringVar(), tk.StringVar()],
    ]
    tiles = tk.Frame(root)
    tiles.pack()
    # add padding between tiles
    tiles.columnconfigure(0, pad=5)
    tiles.columnconfigure(1, pad=5)
    tiles.columnconfigure(2, pad=5)
    tiles.columnconfigure(3, pad=5)
    tiles.columnconfigure(4, pad=5)
    tiles.rowconfigure(0, pad=5)
    tiles.rowconfigure(1, pad=5)
    tiles.rowconfigure(2, pad=5)
    tiles.rowconfigure(3, pad=5)
    tiles.rowconfigure(4, pad=5)
    tiles.rowconfigure(5, pad=5)
    # store labels so that we can change their colouring later
    labels = []
    for i in range(6):
        row = []
        for j in range(5):
            label = tk.Label(tiles, textvariable=tile_vars[i][j], width=6, height=3, relief="ridge")
            label.grid(row=i, column=j)
            row.append(label)
        labels.append(row)
    buttons = tk.Frame(root)
    buttons.pack()
    def execute_key(key):
        # TODO: fix error when letter is clicked after all rows are filled
        # one way to do that is to fix the min(position[1] + 1, 5) to be
        # correctly min(position[1] + 1, 4) but that breaks the backspacing
        # since the last row is ignored
        nonlocal position, is_won
        if key not in ("Enter", "Back"):
            # if it's a letter, set the corresponding tile var
            if not tile_vars[position[0]][position[1]].get():
                tile_vars[position[0]][position[1]].set(key)
            # update the position to be the current row, and the next column
            # the column index is at most 5
            position = position[0], min(position[1] + 1, 5)
        elif key == "Back":
            # empty the tile at the current row, and the previous column
            # the column index is at least 0
            tile_vars[position[0]][max(position[1] - 1, 0)].set("")
            # update the position to the current row, and the previous column
            position = position[0], max(position[1] - 1, 0)
        elif key == "Enter":
            # get the characters from the current row
            guess_chars = [tile_var.get() for tile_var in tile_vars[position[0]]]
            # empty tiles have an empty string in them, which is falsy
            if not all(guess_chars):
                messagebox.showerror("Error", "Not enough letters")
            elif "".join(guess_chars) not in WORD_LIST:
                messagebox.showerror("Error", "Not in word list")
            else:
                row_labels = labels[position[0]]
                for i, guess_char in enumerate(guess_chars):
                    if guess_char not in word_to_guess:
                        row_labels[i].configure(bg="#787c7e", fg="white")
                    else:
                        if guess_char != word_to_guess[i]:
                            row_labels[i].configure(bg="#c9b458", fg="white")
                        else:
                            row_labels[i].configure(bg="#538d4e", fg="white")
                if guess_chars == list(word_to_guess):
                    is_won = True
                    messagebox.showinfo("Congratulations", "You win!")
                else:
                    if position[0] == 5 and not is_won:
                        messagebox.showinfo("Unfortunate", "You lost. The word was " + word_to_guess)
                    # update position to the next row (at most 5), and the
                    # first column
                    position = min(position[0] + 1, 5), 0
    for row in keyboard:
        row_frame = tk.Frame(buttons)
        row_frame.pack()
        row_frame.rowconfigure(0, pad=8)
        for i, key in enumerate(row):
            row_frame.columnconfigure(i, pad=6)
            # the lambda key=key is a common trick to make the closure store
            # the current value of the variable, changing each iteration
            # rather than store a reference to the variable, which is looked up
            # when the function is called, which will be the last iteration's
            # value
            button = tk.Button(row_frame, text=key, font=("arial", 18, "normal"), bg="#d3d6da", command=lambda key=key:execute_key(key))
            button.grid(row=0, column=i)
    root.mainloop()

if __name__ == '__main__':
    main()
